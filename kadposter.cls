% originally by Federico Zenith, federico.zenith@sintef.no.

\ProvidesClass{kadposter}[2022/05/25 Poster class for kad]

% Language options
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{tikzposter}}
\ProcessOptions\relax

\PassOptionsToPackage{cmyk}{xcolor}

\LoadClass[25pt,a3paper,innermargin=5cm]{tikzposter}

\RequirePackage{kadcolor}

\definecolorstyle{kad} {}{
    % Background Colors
    \colorlet{backgroundcolor}{white}
    \colorlet{framecolor}{white}
    % Title Colors
    \colorlet{titlefgcolor}{kaddarkcmyk}
    \colorlet{titlebgcolor}{kaddarkcmyk}
    % Block Colors
    \colorlet{blocktitlebgcolor}{kadlightbluecmyk!7}
    \colorlet{blocktitlefgcolor}{kaddarkcmyk}
    \colorlet{blockbodybgcolor}{kadlightbluecmyk!7}
    \colorlet{blockbodyfgcolor}{kaddarkcmyk}
    % Innerblock Colors
    \colorlet{innerblocktitlefgcolor}{kaddarkcmyk}
    \colorlet{innerblocktitlebgcolor}{kadyellowcmyk}
    \colorlet{innerblockbodybgcolor}{kadyellowcmyk}
    \colorlet{innerblockbodyfgcolor}{kaddarkcmyk}
    % Border to separate sc from background
    \colorlet{screenshotbordercolor}{kaddarkbluecmyk!35}
}

\definebackgroundstyle{kad}{
    \draw[inner sep=0pt, line width=0pt, backgroundcolor, fill=backgroundcolor]
    (bottomleft) rectangle (topright);
    \node[inner sep=0pt] (kad) at
    (0.45\paperwidth-6.4cm,0.5\paperheight-7.5cm)
    {\colorbox{blockbodybgcolor}{\includegraphics[width=11cm]{images/ntnu_transparent.png}}};
}

\definetitlestyle{kad}{
    width=0.9\paperwidth, roundedcorners=0, linewidth=0pt, innersep=0cm,
    titletotopverticalspace=5cm, titletoblockverticalspace=5cm
}{}

\renewcommand\TP@maketitle{%
    \begin{minipage}{0.8\titlewidth}
        \color{titlefgcolor}
        {\hspace{0.7cm}\Huge \@title \par}
        \vspace*{2em}
        {\hspace{0.7cm}\textcolor{kaddark}{\rule{4cm}{2mm}}\par}
        \vspace*{2em}
        {\hspace{0.7cm}\huge \@author \par}
        \vspace*{1em}
        {\hspace{0.7cm}\Large \@institute}
    \end{minipage}
}

\defineblockstyle{kad}{
    titlewidthscale=1, bodywidthscale=1, titleleft,
    titleoffsetx=0pt, titleoffsety=0pt, bodyoffsetx=0mm, bodyoffsety=15mm,
    bodyverticalshift=10mm, roundedcorners=0, linewidth=0pt,
    titleinnersep=1cm, bodyinnersep=1cm
}{
    \draw[color=framecolor, fill=blockbodybgcolor,
        rounded corners=\blockroundedcorners] (blockbody.south west)
    rectangle (blockbody.north east);
    \ifBlockHasTitle
        \draw[color=framecolor, fill=blocktitlebgcolor,
            rounded corners=\blockroundedcorners] (blocktitle.south west) --
        (blocktitle.north west) -- (blocktitle.north east) -- (blocktitle.south
        east);
    \fi
}

\definelayouttheme{kad}{
    \usecolorstyle[colorPalette=kad]{kad}
    \usebackgroundstyle{kad}
    \usetitlestyle{kad}
    \useblockstyle{kad}
}

\usetheme{kad}

\tikzposterlatexaffectionproofoff

\RequirePackage{carlito,tgtermes}
\renewcommand{\familydefault}{\sfdefault}
