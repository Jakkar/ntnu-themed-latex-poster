# NTNU themed LaTeX poster

This repo contains LaTeX files for making a clean and simple poster. 
The theme features an NTNU logo, but which can be easily swapped out.

A very simple example poster is included in [poster.tex](poster.tex).
Check out the [original template](https://www.overleaf.com/latex/templates/sintef-poster/hksprrptfntf) for a more detailed example.

## Credit

This theme is a modified version of Federico Zenith's [SINTEF Poster theme](https://www.overleaf.com/latex/templates/sintef-poster/hksprrptfntf).

Licensed under Creative Commons CC BY 4.0.